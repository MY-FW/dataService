package org.datagroup.dataservice.biz.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataservice.biz.IVerifyConfigBiz;
import org.datagroup.dataservice.dao.entity.VerifyConfig;
import org.datagroup.dataservice.dao.service.IVCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 处理业务逻辑类
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
@Service("verifyConfigBiz")
public class VerifyConfigBizimpl implements IVerifyConfigBiz {

  private static final Logger logger = LogManager.getLogger(VerifyConfigBizimpl.class);

  @Autowired
  IVCService vcService;

  @Override
  public List<VerifyConfig> findVC() {
    return vcService.findVC();
  }

  /**
   * 根据SEIBEL表和新表表名查询配置项
   * 
   * @param sourceTable
   * @param targetTable
   * @return
   */
  @Override
  public List<VerifyConfig> findByParam(String sourceTable, String targetTable) {
    return vcService.findByParam(sourceTable, targetTable);
  }
}
