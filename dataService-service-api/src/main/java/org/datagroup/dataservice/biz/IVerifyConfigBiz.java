package org.datagroup.dataservice.biz;

import java.util.List;

import org.datagroup.dataservice.dao.entity.VerifyConfig;

public interface IVerifyConfigBiz {

  public List<VerifyConfig> findVC();

  /**
   * 根据SEIBEL表和新表表名查询配置项
   * 
   * @param sourceTable
   * @param targetTable
   * @return
   */
  public List<VerifyConfig> findByParam(String sourceTable, String targetTable);
}
