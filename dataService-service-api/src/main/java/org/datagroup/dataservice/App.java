package org.datagroup.dataservice;

import javax.validation.Validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 启动类
 * 
 * @author Chentw
 * @creation 2018年9月28日
 */
@EnableEurekaClient
@RestController
@SpringBootApplication
@EnableCaching
@ComponentScan(basePackages = { "org.datagroup.dataAudit" })
@EnableJpaRepositories("org.datagroup.dataAudit.dao")
@EntityScan("org.datagroup.dataAudit.dao.entity")
public class App {

  private static final Logger logger = LogManager.getLogger(App.class);

  public static void main(String[] args) throws Exception {

    SpringApplication.run(App.class, args);

    // new Thread(new InitThreadPool()).start();
  }

  @Value("${server.port}")
  String port;

  @RequestMapping("/hi")
  public String home(@RequestParam String name) {
    return "hi " + name + ",i am from port:" + port;
  }

  public ResourceBundleMessageSource getMessageSource() throws Exception {
    ResourceBundleMessageSource rbms = new ResourceBundleMessageSource();
    rbms.setDefaultEncoding("UTF-8");
    rbms.setBasenames("i18n/ValidationMessages");
    return rbms;
  }

  @Bean
  public Validator getValidator() throws Exception {
    LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
    validator.setValidationMessageSource(getMessageSource());
    return validator;
  }

}
