package org.datagroup.dataservice.thread;

import java.util.concurrent.ExecutorService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataservice.dao.entity.VerifyConfig;

/**
 * 稽核数据类
 * 
 * @author Chentw
 * @creation 2018年10月13日
 */
public class VerifyDataThreadPool implements Runnable {

  private static final Logger logger = LogManager.getLogger(VerifyDataThreadPool.class);

  private VerifyConfig verifyConfig;
  private ExecutorService newCachedThreadPool;
  private int nextCreateThread;

  // ConcurrentMap<, V> map = new ConcurrentHashMap<K, V>();

  public VerifyDataThreadPool() {
  }

  @Override
  public void run() {
    try {
      int count = 1;
      while (!newCachedThreadPool.isShutdown()) {
        // 数据稽核核心处理代码块，数据稽核过程可拆分为多个方法，避免单个方法代码过长
        // 另，数据查询等业务逻辑处理尽量在biz层处理。

        logger.info("开始第{}批第{}次稽核数据啦", nextCreateThread, count++);
        Thread.sleep(5000L);
      }
    } catch (Exception e) {
      logger.debug("执行数据稽核出错。源表名：{},源表字段：{},目标表名：{},目标表字段：{},存储过程名：{},原因:{}", verifyConfig.getSource_table(), verifyConfig.getSource_fields(), verifyConfig.getTarget_table(), verifyConfig
          .getTarget_fields(), verifyConfig.getProcess_name(), e.getStackTrace());
    }
  }

}
