package org.datagroup.dataservice.thread;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataservice.biz.IVerifyConfigBiz;
import org.datagroup.dataservice.config.ApplicationContextProvider;
import org.datagroup.dataservice.dao.entity.VerifyConfig;

/**
 * 初始化配置表信息，每隔半个小时重新加载配置表数据， 并重新创建线程池，并根据配置表中数据量启动线程。说明：该类做法有缺点，需优化
 * 
 * @author Chentw
 * @creation 2018年10月12日
 */

public class InitThreadPool extends Thread {
  private static final Logger logger = LogManager.getLogger(InitThreadPool.class);

  private static ExecutorService newCachedThreadPool = Executors.newCachedThreadPool();;
  private int nextCreateThread = 1;
  private IVerifyConfigBiz verifyConfigBiz = (IVerifyConfigBiz) new ApplicationContextProvider().getBean("verifyConfigBiz");

  @Override
  public void run() {
    while (true) {
      try {
        initNewCachedThreadPool();
        // Thread.sleep(1800000L); // 每30分钟重新加载配置表数据，并重新创建线程池
        Thread.sleep(1000L);
        newCachedThreadPool.shutdown();
      } catch (Exception e) {
        logger.error("启动线程出错啦!原因:{}", e.getStackTrace());
      }
    }
  }

  /**
   * 根据配置表中数据，创建线程池、处理线程
   */
  public void initNewCachedThreadPool() {
    List<VerifyConfig> list = initVerifyConfig();
    if (newCachedThreadPool == null || newCachedThreadPool.isShutdown()) {
      newCachedThreadPool = Executors.newCachedThreadPool();
      nextCreateThread++;
    }
    try {
      for (int i = 0; i < list.size(); i++) {
        newCachedThreadPool.submit(new VerifyDataThread(list.get(i), newCachedThreadPool, nextCreateThread));
      }
      logger.info("第{}次启动线程已完成，本次共启动{}个线程。", nextCreateThread, list.size());
    } catch (Exception e) {
      logger.error("第{}次启动线程失败,原因:{}", nextCreateThread, e.getStackTrace());
    }
  }

  /**
   * 全量加载配置表信息
   * 
   * @return
   */
  public List<VerifyConfig> initVerifyConfig() {
    try {
      List<VerifyConfig> list = verifyConfigBiz.findVC();
      return list;
    } catch (Exception e) {
      logger.debug("初始化稽核配置表信息出错。原因:{}", e.getStackTrace());
    }
    return null;
  }

}
