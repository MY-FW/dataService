package org.datagroup.dataservice.config;

import lombok.Data;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "dataaudit.config")
@PropertySource(value = "classpath:dataAudit.properties", encoding = "UTF-8")
public class DataAuditConfigProperty {
  private String name;
  private String count;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCount() {
    return count;
  }

  public void setCount(String count) {
    this.count = count;
  }

}
