
-- Create table
create table VERIFY_CONFIG
(
  source_table      VARCHAR2(40),
  target_table      VARCHAR2(40),
  process_name      VARCHAR2(200),
  verify_start_time DATE,
  verify_end_time   DATE,
  verify_status     VARCHAR2(10),
  audi_start_time   DATE,
  audi_end_time     DATE,
  remarks           VARCHAR2(100),
  item_id           VARCHAR2(30) not null,
  source_fields     VARCHAR2(500),
  target_fields     VARCHAR2(500),
  status            NUMBER(2)
)
tablespace SBL_DATA_TBS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column VERIFY_CONFIG.verify_start_time
  is '稽核范围起始时间';
comment on column VERIFY_CONFIG.verify_end_time
  is '稽核范围终止时间';
comment on column VERIFY_CONFIG.verify_status
  is '状态，70A待开始，70B稽核中，70C稽核完成，70E稽核异常';
comment on column VERIFY_CONFIG.audi_start_time
  is '执行开始时间';
comment on column VERIFY_CONFIG.audi_end_time
  is '执行结束时间';
comment on column VERIFY_CONFIG.remarks
  is '备注说明';
comment on column VERIFY_CONFIG.item_id
  is '主键';
-- Create/Recreate primary, unique and foreign key constraints 
alter table VERIFY_CONFIG
  add constraint ITEM_ID_PK primary key (ITEM_ID)
  using index 
  tablespace SBL_DATA_TBS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );


prompt Importing table VERIFY_CONFIG...
set feedback off
set define off
insert into VERIFY_CONFIG (SOURCE_TABLE, TARGET_TABLE, PROCESS_NAME, VERIFY_START_TIME, VERIFY_END_TIME, VERIFY_STATUS, AUDI_START_TIME, AUDI_END_TIME, REMARKS, ITEM_ID, SOURCE_FIELDS, TARGET_FIELDS, STATUS)
values ('SIEBEL.S_ORG_EXT', 'CUSTOMER', 'JH_FN_YANNANNNA.F_S_ORG_EXT2CUSTOMER', to_date('01-10-2018', 'dd-mm-yyyy'), to_date('02-10-2018', 'dd-mm-yyyy'), '70A', to_date('11-10-2018 14:51:51', 'dd-mm-yyyy hh24:mi:ss'), null, null, '1', null, 'CUST_NUMBER,CUST_TYPE,ENTER_DATE,EXT_CUST_ID,GROUP_CUST_ID,PARTY_ID,TAX_PAYER_ID,IS_REALNAME', 0);

prompt Done.

