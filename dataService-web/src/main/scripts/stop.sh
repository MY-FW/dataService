#!/bin/sh

PID=`ps ux |grep "org.datagroup.dataAudit.service.App" |grep -v grep|awk '{print $2}'`
if [ ! "$PID" ]; then
  echo "Process is not running!"
  exit 1
else
  echo "Process[$PID] stopped!"
  kill -9 $PID
fi
