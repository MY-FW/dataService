#!/bin/env bash
# this is start script for the server

# 1. check JAVA_HOME
if [ -z "${JAVA_HOME}" ]; then
  echo "pls set JAVA_HOME first"
  exit 1
fi

# 2. check process exist
MAINCLASS="org.datagroup.dataAudit.service.App"
PID=`ps ux |grep ${MAINCLASS} |grep -v grep|awk '{print $2}'`
if [ "$PID" ]; then
  echo "Process[$PID] is already running!"
  exit 1
fi

# 3. set vm options
HOST=`hostname -i`
cd "`dirname $0`/.."
APP_HOME=`pwd`
LOG_PATH="${APP_HOME}/logs"
COREDUMP_PATH="${APP_HOME}/dumps"
ETC_PATH="${APP_HOME}/etc"

mkdir -p ${COREDUMP_PATH}

JMX_PORT=8123
MIN_MEM=1024m
MAX_MEM=4048m

liblist=.
for jar in "${APP_HOME}/lib/"*.jar
do
  liblist="$liblist:$jar"
done

CLASSPATH="-classpath ${liblist}:${ETCPATH} "
JAVA_OPTS="-server -Xms${MIN_MEM} -Xmx${MAX_MEM} "
JMX_OPTS="-Djava.rmi.server.hostname=${HOST} -Dcom.sun.management.jmxremote.port=${JMX_PORT} -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false "
CORE_OPTS="-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${COREDUMP_PATH}"
APP_OPTS="-Dlog4j.configurationFile=${APP_HOME}/etc/log4j2.xml -Dspring.config.location=${ETC_PATH}/application.properties -DconfPath=${APP_HOME}/etc -DlogPath=${LOG_PATH}"

ALL_JAVA_OPTIONS="${JAVA_OPTS} ${CLASSPATH} ${CORE_OPTS} ${APP_OPTS} "

nohup ${JAVA_HOME}/bin/java ${ALL_JAVA_OPTIONS}  ${MAINCLASS} >logs/nohup.out &
