package org.datagroup.dataservice.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataservice.web.po.VerifyConfig;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

/**
 * Http
 * 
 * @author Chentw
 * @creation 2018年9月28日
 */
@Controller
public class HttpRequestController {

  private static final Logger logger = LogManager.getLogger(HttpRequestController.class);

  private static final Pattern PATTERN = Pattern.compile("^1\\d{10}$");

  // @Autowired
  // private IVerifyConfigBiz verifyConfigBiz;
  // @Autowired
  // private DataAuditConfigProperty configProperty;

  // @ResponseBody
  @RequestMapping("/index")
  public String index() {
    return "index";
  }

  // @ResponseBody
  @RequestMapping("/find")
  public String find() {
    return "vcList";
  }

  @ResponseBody
  @RequestMapping("/findAll")
  public String findAll() throws InterruptedException {
    List<VerifyConfig> vcList = new ArrayList<VerifyConfig>();
    VerifyConfig vc = new VerifyConfig();
    vc.setItemId("1");
    vc.setSourceTable("test");
    vc.setSourceFields("test11");
    vc.setTargetFields("test22");
    vc.setTargetTable("tests");
    VerifyConfig vc1 = new VerifyConfig();
    vc1.setItemId("2");
    vc1.setSourceTable("test1");
    vc1.setSourceFields("test111");
    vc1.setTargetFields("test222");
    vc1.setTargetTable("testss");
    vcList.add(vc1);
    vcList.add(vc);
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("code", 0);
    jsonObject.put("msg", "");
    jsonObject.put("count", 2);
    jsonObject.put("data", vcList);
    logger.info("查询结果为：{}", vc);
    return jsonObject.toJSONString();
  }

  /**
   * 根据入参查询
   * 
   * @return
   */
  /*
   * @ResponseBody
   * 
   * @GetMapping("/findByParam") public List<VerifyConfig> findByParam() {
   * List<VerifyConfig> list = verifyConfigBiz.findByParam("S_ORG_EXT",
   * "CUSTOMER"); logger.info("输出结果为：{}", list); return list; }
   *//**
   * 使用jdbcTemplate查询数据
   * 
   * @return
   */
  /*
   * @ResponseBody
   * 
   * @GetMapping("/findAllList") public List<Map<String, Object>> findAllList()
   * { String sql = "select * from verify_config"; List<Map<String, Object>>
   * list = null; return list; }
   *//**
   * 有入参，返回值存储过程调用
   * 
   * @return
   */
  /*
   * @ResponseBody
   * 
   * @GetMapping("/queryCall") public List queryCall() { return null; }
   *//**
   * 输出配置文件数据
   * 
   * @return
   */
  /*
   * @ResponseBody
   * 
   * @GetMapping("/queryConfig") public Map<String, String> configProperty() {
   * Map<String, String> map = new HashMap<String, String>(); map.put("name",
   * configProperty.getName()); map.put("count", configProperty.getCount());
   * return map;
   * 
   * }
   */

}
