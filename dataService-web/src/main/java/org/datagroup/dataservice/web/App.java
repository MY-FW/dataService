package org.datagroup.dataservice.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * 该工程为WEB工程。
 * 
 * @author Chentw
 * @creation 2018年10月19日
 */
@EnableDiscoveryClient
@EnableFeignClients
@SpringBootApplication
public class App {
  private static final Logger logger = LogManager.getLogger(App.class);

  public static void main(String[] args) {

    SpringApplication.run(App.class, args);

  }
}
