package org.datagroup.dataservice.web.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataservice.web.FeignClient.SchedualServiceHi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiController {

  private static final Logger logger = LogManager.getLogger(HiController.class);

  @Autowired
  private DiscoveryClient client;
  @Autowired
  SchedualServiceHi schedualServiceHi;

  @RequestMapping(value = "/hi", method = RequestMethod.GET)
  public String sayHi(@RequestParam String name) {
    return schedualServiceHi.sayHiFromClientOne(name);
  }

  /**
   * 使用Discovery实现服务调用。需要开启Discovery注解（@EnableDiscoveryClient）。
   * 
   * @return
   */
  @RequestMapping(value = "/hello", method = RequestMethod.GET)
  public String index() {
    List<ServiceInstance> instances = client.getInstances("dataAudit-services-api");
    for (int i = 0; i < instances.size(); i++) {
      logger.info("/hello,host:" + instances.get(i).getHost() + ",service_id:" + instances.get(i).getServiceId());
    }
    return "Hello World";
  }

}
