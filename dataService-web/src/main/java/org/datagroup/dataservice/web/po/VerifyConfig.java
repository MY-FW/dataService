package org.datagroup.dataservice.web.po;

public class VerifyConfig {
  private String itemId;
  private String sourceTable;// 配置项增量ogg表名
  private String sourceFields;// 配置项增量ogg字段名
  private String targetTable;// 源目标表名
  private String targetFields;// 源目标表字段

  // private String process_name;// 存储过程名称
  // private Date verify_start_time;// 稽核开始时间
  // private Date verify_end_time;// 稽核结束时间
  // private String verify_status;// 增量配置稽核状态【状态：70A待开始，70B稽核中，70C稽核完成，70E稽核异常】
  // private Date audi_start_time;// 执行开始时间
  // private Date audi_end_time;// 执行结束时间
  // private String status;
  // private String remarks;
  public String getItemId() {
    return itemId;
  }

  public void setItemId(String itemId) {
    this.itemId = itemId;
  }

  public String getSourceTable() {
    return sourceTable;
  }

  public void setSourceTable(String sourceTable) {
    this.sourceTable = sourceTable;
  }

  public String getSourceFields() {
    return sourceFields;
  }

  public void setSourceFields(String sourceFields) {
    this.sourceFields = sourceFields;
  }

  public String getTargetTable() {
    return targetTable;
  }

  public void setTargetTable(String targetTable) {
    this.targetTable = targetTable;
  }

  public String getTargetFields() {
    return targetFields;
  }

  public void setTargetFields(String targetFields) {
    this.targetFields = targetFields;
  }

}
