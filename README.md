# dataAudits   

#### 项目介绍

该项目用于Oracle到mysql增量数据稽核功能.
1. 查询Oracle数据中配置表信息（表明：VERIFY_CONFIG）.
2. 根据配置表中原始表字段（SOURCE_TABLE）查询对应OGG(OGG增量日志表为：配置表中字段（SOURCE_TABLE）值加“_INC”)日志表表中增量数据（其中包括ROW_ID）.
3. 根据ROW_ID,调用配置表中对应的存储过程（字段名：PROCESS_NAME）.
4. 再根据存储过程返回结果、配置表中新表名（字段：TARGET_TABLE）查询mysql中对应表中数据.
5. 将ORACLE存储过程解析的数据与MYSQL中查询的数据作对比.
6. 将对比结果更新至稽核日志表中，并更新OGG增量表中已稽核完成的数据.

#### 软件架构

软件架构说明：
1. 项目采用java语言编写.
2. 技术框架目采用SpringBoot+DUBBO+Zookeeper+Druid+Jpa+Maven.

#### 安装教程

1. 打开DOS命令行模式，切换至项目根路径，执行MVN install即可完成.
2. 解压打包后的*.zip文件.
3. 修改配置文件数据源.
4. 执行start.sh

#### 使用说明

1. 略

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

