package org.datagroup.dataservice.dao.entity;

import java.sql.Date;

//@Entity
//@Table(name = "")
public class SiebelIncPojo {
  private String table_name;
  private String row_id;
  private String op_type;
  private String status_cd;// 状态，70A待开始，70B稽核中，70C稽核完成，70E稽核异常
  private Date create_date;
  private Date status_date;
  private Date last_upd;

  public SiebelIncPojo(String table_name, String row_id, String op_type, String status_cd, Date create_date, Date status_date, Date last_upd) {
    super();
    this.table_name = table_name;
    this.row_id = row_id;
    this.op_type = op_type;
    this.status_cd = status_cd;
    this.create_date = create_date;
    this.status_date = status_date;
    this.last_upd = last_upd;
  }

  public String getTable_name() {
    return table_name;
  }

  public void setTable_name(String table_name) {
    this.table_name = table_name;
  }

  public String getRow_id() {
    return row_id;
  }

  public void setRow_id(String row_id) {
    this.row_id = row_id;
  }

  public String getOp_type() {
    return op_type;
  }

  public void setOp_type(String op_type) {
    this.op_type = op_type;
  }

  public String getStatus_cd() {
    return status_cd;
  }

  public void setStatus_cd(String status_cd) {
    this.status_cd = status_cd;
  }

  public Date getCreate_date() {
    return create_date;
  }

  public void setCreate_date(Date create_date) {
    this.create_date = create_date;
  }

  public Date getStatus_date() {
    return status_date;
  }

  public void setStatus_date(Date status_date) {
    this.status_date = status_date;
  }

  public Date getLast_upd() {
    return last_upd;
  }

  public void setLast_upd(Date last_upd) {
    this.last_upd = last_upd;
  }
}
