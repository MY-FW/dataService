package org.datagroup.dataservice.dao.service;

import java.util.List;

import org.datagroup.dataservice.dao.entity.VerifyConfig;
import org.springframework.data.repository.query.Param;

public interface IVCService {

  List<VerifyConfig> findVC();

  List<VerifyConfig> findByParam(String sourceTable, String targetTable);

  List<VerifyConfig> findById(@Param("itemId") String itemId);
}
