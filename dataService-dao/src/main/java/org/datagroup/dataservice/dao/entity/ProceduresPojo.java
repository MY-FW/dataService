package org.datagroup.dataservice.dao.entity;

import java.io.Serializable;

//@Entity
//@Table(name = "")
public class ProceduresPojo implements Serializable {
  private String row_id;// 数据唯一标识
  private String table_row_id;// 源oracle目标表row_id
  private String table_name;// 源oracle目标表
  private String siebel_table_col;// 源siebel表字段
  private String siebel_table_value;// 源siebel表值
  private String crm_table_col;// crm表字段
  private String crm_table_value;// crm表值

  public ProceduresPojo() {
    super();
  }

  public ProceduresPojo(String row_id, String table_row_id, String table_name, String siebel_table_col, String siebel_table_value, String crm_table_col, String crm_table_value) {
    super();
    this.row_id = row_id;
    this.table_row_id = table_row_id;
    this.table_name = table_name;
    this.siebel_table_col = siebel_table_col;
    this.siebel_table_value = siebel_table_value;
    this.crm_table_col = crm_table_col;
    this.crm_table_value = crm_table_value;
  }

  public String getRow_id() {
    return row_id;
  }

  public void setRow_id(String row_id) {
    this.row_id = row_id;
  }

  public String getTable_row_id() {
    return table_row_id;
  }

  public void setTable_row_id(String table_row_id) {
    this.table_row_id = table_row_id;
  }

  public String getTable_name() {
    return table_name;
  }

  public void setTable_name(String table_name) {
    this.table_name = table_name;
  }

  public String getSiebel_table_col() {
    return siebel_table_col;
  }

  public void setSiebel_table_col(String siebel_table_col) {
    this.siebel_table_col = siebel_table_col;
  }

  public String getSiebel_table_value() {
    return siebel_table_value;
  }

  public void setSiebel_table_value(String siebel_table_value) {
    this.siebel_table_value = siebel_table_value;
  }

  public String getCrm_table_col() {
    return crm_table_col;
  }

  public void setCrm_table_col(String crm_table_col) {
    this.crm_table_col = crm_table_col;
  }

  public String getCrm_table_value() {
    return crm_table_value;
  }

  public void setCrm_table_value(String crm_table_value) {
    this.crm_table_value = crm_table_value;
  }

}
