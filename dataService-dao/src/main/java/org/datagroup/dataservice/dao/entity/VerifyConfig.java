package org.datagroup.dataservice.dao.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VERIFY_CONFIG")
public class VerifyConfig implements Serializable {
  @Id
  private String item_id;
  private String source_table;// 配置项增量ogg表名
  private String source_fields;// 配置项增量ogg字段名
  private String target_table;// 源目标表名
  private String target_fields;// 源目标表字段
  private String process_name;// 存储过程名称
  private Date verify_start_time;// 稽核开始时间
  private Date verify_end_time;// 稽核结束时间
  private String verify_status;// 增量配置稽核状态【状态：70A待开始，70B稽核中，70C稽核完成，70E稽核异常】
  private Date audi_start_time;// 执行开始时间
  private Date audi_end_time;// 执行结束时间
  private String status;
  private String remarks;

  public VerifyConfig() {
    super();
  }

  public VerifyConfig(String source_table, String source_fields, String target_table, String target_fields, String process_name, Date verify_start_time, Date verify_end_time,
      String verify_status, Date audi_start_time, Date audi_end_time) {
    super();
    this.source_table = source_table;
    this.source_fields = source_fields;
    this.target_table = target_table;
    this.target_fields = target_fields;
    this.process_name = process_name;
    this.verify_start_time = verify_start_time;
    this.verify_end_time = verify_end_time;
    this.verify_status = verify_status;
    this.audi_start_time = audi_start_time;
    this.audi_end_time = audi_end_time;
  }

  public String getSource_table() {
    return source_table;
  }

  public void setSource_table(String source_table) {
    this.source_table = source_table;
  }

  public String getSource_fields() {
    return source_fields;
  }

  public void setSource_fields(String source_fields) {
    this.source_fields = source_fields;
  }

  public String getTarget_table() {
    return target_table;
  }

  public void setTarget_table(String target_table) {
    this.target_table = target_table;
  }

  public String getTarget_fields() {
    return target_fields;
  }

  public void setTarget_fields(String target_field) {
    this.target_fields = target_field;
  }

  public String getProcess_name() {
    return process_name;
  }

  public void setProcess_name(String process_name) {
    this.process_name = process_name;
  }

  public Date getVerify_start_time() {
    return verify_start_time;
  }

  public void setVerify_start_time(Date verify_start_time) {
    this.verify_start_time = verify_start_time;
  }

  public Date getVerify_end_time() {
    return verify_end_time;
  }

  public void setVerify_end_time(Date verify_end_time) {
    this.verify_end_time = verify_end_time;
  }

  public String getVerify_status() {
    return verify_status;
  }

  public void setVerify_status(String verify_status) {
    this.verify_status = verify_status;
  }

  public Date getAudi_start_time() {
    return audi_start_time;
  }

  public void setAudi_start_time(Date audi_start_time) {
    this.audi_start_time = audi_start_time;
  }

  public Date getAudi_end_time() {
    return audi_end_time;
  }

  public void setAudi_end_time(Date audi_end_time) {
    this.audi_end_time = audi_end_time;
  }

  public String getItem_id() {
    return item_id;
  }

  public void setItem_id(String item_id) {
    this.item_id = item_id;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

}
