package org.datagroup.dataservice.dao.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.datagroup.dataservice.dao.entity.VerifyConfig;
import org.datagroup.dataservice.dao.repository.impl.VCRepository;
import org.datagroup.dataservice.dao.service.IVCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

/**
 * 处理查询业务逻辑类，已开启事物控制
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
@Service("vcService")
@Transactional
public class VCServiceImpl implements IVCService {

  private static final Logger logger = LogManager.getLogger(VCServiceImpl.class);

  // private final LoadingCache<String, String> cache;
  // private final Cache<String, String> cache;

  @Autowired
  private VCRepository vcRepository;

  // public VCServiceImpl() {
  // cache = CacheBuilder.newBuilder().expireAfterWrite(10,
  // TimeUnit.SECONDS).build();
  // }

  // public VCServiceImpl() {
  // cache = CacheBuilder.newBuilder().expireAfterWrite(5,
  // TimeUnit.SECONDS).build(new CacheLoader<String, String>() {
  // @Override
  // public String load(String key) throws Exception {
  // // TODO Auto-generated method stub
  // }
  // });
  // }

  /**
   * 查询配置表中所有数据
   * 
   * @return
   */
  @Override
  public List<VerifyConfig> findVC() {
    return vcRepository.findVC();
  }

  /**
   * 根据SEIBEL表和新表表名查询配置项
   * 
   * @param sourceTable
   * @param targetTable
   * @return
   */
  @Override
  public List<VerifyConfig> findByParam(String sourceTable, String targetTable) {
    return vcRepository.findByParam(sourceTable, targetTable);
  }

  /**
   * 根据主键（item_id） 查询配置表中数据
   * 
   * @param itemId
   * @return
   */
  @Override
  public List<VerifyConfig> findById(@Param("itemId") String itemId) {
    return vcRepository.findById(itemId);
  }

}
