package org.datagroup.dataservice.dao.repository.impl;

import java.util.List;

import org.datagroup.dataservice.dao.entity.VerifyConfig;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * 查询配置表（VerifyConfig）数据，并开启缓存
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
@Repository
@CacheConfig(cacheNames = "verifyConfig")
public interface VCRepository extends JpaRepository<VerifyConfig, Integer> {
  // final LoadingCache<String,String> cache;
  // @Cacheable(value = "verifyConfig#${select.cache.timeout:1000}", key = "")
  // { "List<VerifyConfig>#10#2" }
  // @CacheEvict(value = "verifyConfig", allEntries = false)
  // @Scheduled(fixedDelay = 5000)
  // cache = CacheBuilder.newBuilder().expireAfterWrite(5,
  // TimeUnit.SECONDS).build();
  // cache = CacheBuilder.newBuilder().expireAfterWrite(5,
  // TimeUnit.SECONDS).build(new CacheLoader<String, String>() {
  // public String load(String id) throws Exception
  // {
  // System.out.println("method inovke");
  // //这里执行查询数据库，等其他复杂的逻辑
  // return "User:" + id;
  // }
  // });
  // }
  @Cacheable
  @Query("select e from VerifyConfig e")
  List<VerifyConfig> findVC();

  @Cacheable
  @Query("select e from VerifyConfig e where e.source_table =:st and e.target_table =:tt")
  List<VerifyConfig> findByParam(@Param("st") String sourceTable, @Param("tt") String targetTable);

  @Cacheable
  @Query(value = "select e from VerifyConfig e where e.item_id = :itemId")
  List<VerifyConfig> findById(@Param("itemId") String itemId);

  // @CacheEvict
  // @Query(value = "", nativeQuery = true)
  // void editVerifyConfig();

  // @CacheEvict
  // @Query(value = "")
  // void addVerifyConfig();
}
