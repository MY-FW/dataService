package org.datagroup.dataservice.dao.repository;

import java.util.List;

/**
 * 调用存储过程，该接口类使用两种方式实现
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
public interface ICellRepository {

  public void queryCall(String param);

  public List queryCallAll(String param);
}
