package org.datagroup.dataservice.dao.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.datagroup.dataservice.dao.entity.VerifyConfig;
import org.datagroup.dataservice.dao.repository.ICellRepository;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

/**
 * JPA存储过程调用
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
@Repository
public class CallRepository implements ICellRepository {

  @PersistenceContext
  private EntityManager em;

  /**
   * 调用返回值存储过程
   * 
   * @param param
   */
  @Override
  public void queryCall(String param) {
    this.em.createNativeQuery("").setParameter("", param).executeUpdate();
  }

  /**
   * 调用有返回值存储过程
   * 
   * @param param
   * @return
   */
  @Override
  public List queryCallAll(String param) {
    Query query = em.createNativeQuery("{call GetPersonList()}", VerifyConfig.class).setParameter("", param);
    query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
    // query.setHint(QueryHints.RESULT_TYPE, ResultType.Map);
    // Query query = em.createNativeQuery("", java.util.Map.class);
    // Session session = em.getEntityManagerFactory().unwrap(Session.class);
    // return
    // session.createQuery("").setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
    List result = query.getResultList();
    return result;
  }

}
