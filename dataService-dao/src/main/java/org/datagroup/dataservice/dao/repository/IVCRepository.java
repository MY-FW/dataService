package org.datagroup.dataservice.dao.repository;

import java.util.List;

import org.datagroup.dataservice.dao.entity.VerifyConfig;
import org.springframework.data.repository.query.Param;

/**
 * 查询配置表（VerifyConfig）数据，并开启缓存
 * 
 * @author Chentw
 * @creation 2018年10月11日
 */
public interface IVCRepository {

  List<VerifyConfig> findVC();

  List<VerifyConfig> findByParam(@Param("st") String sourceTable, @Param("tt") String targetTable);

}
