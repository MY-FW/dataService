package org.datagroup.dataservice.dao.dbconfig;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;

/**
 * 多数据源，集成druid,配置监控
 * 
 * @author Chentw
 * @creation 2018年9月28日
 */
@Configuration
@EnableTransactionManagement
public class DataSourceFactory {
  private static final Logger logger = LogManager.getLogger(DataSourceFactory.class);

  @Primary
  @Bean(name = "oracleDataSource")
  @Qualifier("oracleDataSource")
  @ConfigurationProperties(prefix = "spring.datasource.oracle")
  public DataSource oracleDataSource(Environment environment) {
    return DruidDataSourceBuilder.create().build(environment, "spring.datasource.druid.");
  }

  @Bean(name = "mysqlDataSource")
  @Qualifier("mysqlDataSource")
  @ConfigurationProperties(prefix = "spring.datasource.mysql")
  public DataSource mysqlDataSource(Environment environment) {
    return DruidDataSourceBuilder.create().build(environment, "spring.datasource.druid.");
  }

  @Bean(name = "oracleJdbcTemplate")
  public JdbcTemplate oracleJdbcTemplate(@Qualifier("oracleDataSource") DataSource dataSource) {
    return new JdbcTemplate(dataSource);
  }

  @Bean(name = "mysqlJdbcTemplate")
  public JdbcTemplate mysqlJdbcTemplate(@Qualifier("mysqlDataSource") DataSource dataSource) {
    return new JdbcTemplate(dataSource);
  }

}
