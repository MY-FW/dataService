package org.datagroup.dataservice.common.generator;

/**
 * 基于Twitter的snowflake算法生成唯一id
 * Twitter-Snowflake算法产生的背景相当简单，为了满足Twitter每秒上万条消息的请求，每条消息都必须分配一条唯一的id，
 * 这些id还需要一些大致的顺序（方便客户端排序），并且在分布式系统中不同机器产生的id必须不同。
 *
 * ID 生成规则: ID长达 64 bits
 *
 * | 41 bits: Timestamp (毫秒) | 4 bits: 部署环境 | 3 bits: 区域（机房） | 6 bits: 机器编号 | 10
 * bits: 序列号 |
 */
public class IdGenerator {
  // 基准时间（41位最多能表达69.7年）
  private long epoch = 1451577600000L; // 2016年 01月 01日 星期五 00:00:00 CST
  // 环境标志位数
  private final static long envIdBits = 4L;
  // 区域标志位数
  private final static long regionIdBits = 3L;
  // 机器标识位数
  private final static long workerIdBits = 6L;
  // 序列号识位数
  private final static long sequenceBits = 10L;

  // 环境标志ID最大值
  private final static long maxEnvId = ~(-1L << envIdBits);
  // 区域标志ID最大值
  private final static long maxRegionId = ~(-1L << regionIdBits);
  // 机器ID最大值
  private final static long maxWorkerId = ~(-1L << workerIdBits);
  // 序列号ID最大值
  private final static long sequenceMask = ~(-1L << sequenceBits);

  // 机器ID偏左移10位
  private final static long workerIdShift = sequenceBits;
  // 业务ID偏左移16位
  private final static long regionIdShift = sequenceBits + workerIdBits;
  // 环境ID左移19位
  private final static long envIdShift = sequenceBits + workerIdBits + regionIdBits;
  // 时间毫秒左移23位
  private final static long timestampLeftShift = sequenceBits + workerIdBits + regionIdBits + envIdBits;

  private static long lastTimestamp = -1L;

  private long sequence = 0L;
  private long workerId;
  private long regionId;
  private long envId;

  public void setWorkerId(long workerId) {
    this.workerId = workerId;
  }

  public void setRegionId(long regionId) {
    this.regionId = regionId;
  }

  public void setEnvId(long envId) {
    this.envId = envId;
  }

  public IdGenerator(long workerId, long regionId, long envId) {
    // 如果超出范围就抛出异常
    if (workerId > maxWorkerId || workerId < 0) {
      throw new IllegalArgumentException("worker Id can't be greater than %d or less than 0");
    }
    if (regionId > maxRegionId || regionId < 0) {
      throw new IllegalArgumentException("datacenter Id can't be greater than %d or less than 0");
    }
    if (envId > maxEnvId || envId < 0) {
      throw new IllegalArgumentException("environment Id can't be greater than %d or less than 0");
    }

    this.workerId = workerId;
    this.regionId = regionId;
    this.envId = envId;
  }

  public IdGenerator(long workerId, long regionId) {

    // 如果超出范围就抛出异常
    if (workerId > maxWorkerId || workerId < 0) {
      throw new IllegalArgumentException("worker Id can't be greater than %d or less than 0");
    }
    if (regionId > maxRegionId || regionId < 0) {
      throw new IllegalArgumentException("datacenter Id can't be greater than %d or less than 0");
    }

    this.workerId = workerId;
    this.regionId = regionId;
    this.envId = 1;
  }

  public IdGenerator(long workerId) {
    // 如果超出范围就抛出异常
    if (workerId > maxWorkerId || workerId < 0) {
      throw new IllegalArgumentException("worker Id can't be greater than %d or less than 0");
    }
    this.workerId = workerId;
    this.regionId = 1;
    this.envId = 1;
  }

  public long generate() {
    return this.nextId(false, 0);
  }

  /**
   * 实际产生代码的
   *
   * @param isPadding
   * @param busId
   * @return
   */
  private synchronized long nextId(boolean isPadding, long busId) {

    long timestamp = timeGen();
    long paddingnum = regionId;

    if (isPadding) {
      paddingnum = busId;
    }

    if (timestamp < lastTimestamp) {
      try {
        throw new Exception("Clock moved backwards.  Refusing to generate id for " + (lastTimestamp - timestamp) + " milliseconds");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    // 如果上次生成时间和当前时间相同,在同一毫秒内
    if (lastTimestamp == timestamp) {
      // sequence自增，因为sequence只有10bit，所以和sequenceMask相与一下，去掉高位
      sequence = (sequence + 1) & sequenceMask;
      // 判断是否溢出,也就是每毫秒内超过1024，当为1024时，与sequenceMask相与，sequence就等于0
      if (sequence == 0) {
        // 自旋等待到下一毫秒
        timestamp = tailNextMillis(lastTimestamp);
      }
    } else {
      sequence = 0;
    }

    lastTimestamp = timestamp;

    return ((timestamp - epoch) << timestampLeftShift) | (envId << envIdShift) | (paddingnum << regionIdShift) | (workerId << workerIdShift) | sequence;
  }

  // 防止产生的时间比之前的时间还要小（由于NTP回拨等问题）,保持增量的趋势.
  private long tailNextMillis(final long lastTimestamp) {
    long timestamp = this.timeGen();
    while (timestamp <= lastTimestamp) {
      timestamp = this.timeGen();
    }
    return timestamp;
  }

  // 获取当前的时间戳
  protected long timeGen() {
    return System.currentTimeMillis();
  }

}
