package org.datagroup.dataservice.common.generator;

import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class RandomUtil {
  public static final String ALLCHAR = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  public static final String NUMBERCHAR = "34567";
  private static AtomicLong uuid = new AtomicLong(createRandom());

  /**
   * 生成小于8位的随机数
   * 
   * @return
   */
  public static long createRandom() {
    StringBuilder sb = new StringBuilder();
    Random rand = new Random();
    int generateNum = generateInt();
    for (int i = 0; i < generateNum; i++) {
      sb.append(rand.nextInt(10));
    }
    return Long.parseLong(sb.toString());
  }

  /**
   * 以随机数为基数，生成自增数
   * 
   * @return
   */
  public static long createUid() {
    return uuid.incrementAndGet();
  }

  /**
   * 返回一个指定随机数
   * 
   * @return
   */
  public static int generateInt() {
    StringBuffer strbuf = new StringBuffer();
    Random random = new Random();
    strbuf.append(NUMBERCHAR.charAt(random.nextInt(5)));
    return Integer.parseInt(strbuf.toString());
  }

  /**
   * 生成小于8位的随机数
   * 
   * @param numberFlag
   * @param length
   * @return
   */
  public static String createRandom(boolean numberFlag, int length) {
    StringBuilder sb = new StringBuilder();
    String strTable = numberFlag ? NUMBERCHAR : ALLCHAR;
    int len = strTable.length();
    boolean bDone = true;
    do {
      int count = 0;
      for (int i = 0; i < length; i++) {
        double dblR = Math.random() * len;
        int intR = (int) Math.floor(dblR);
        char c = strTable.charAt(intR);
        if (('0' <= c) && (c <= '9')) {
          count++;
        }
        sb.append(strTable.charAt(intR));
      }
      if (count >= 2) {
        bDone = false;
      }
    } while (bDone);
    return sb.toString();
  }

  /**
   * 传入字符串如果以零开始，则替换为非零随机数
   * 
   * @param num
   *          数字
   * @param fixdlenth
   *          字符串长度
   * @return 定长的字符串
   */
  public static Long toFixdLengthStr(String fixdLengthStr, int fixdlenth, Random random) {
    if (fixdLengthStr.startsWith("0")) {
      fixdLengthStr = NUMBERCHAR.charAt(random.nextInt(fixdlenth)) + fixdLengthStr.substring(1);
    }
    return Long.parseLong(fixdLengthStr);
  }

  /**
   * 根据数字生成一个定长整形，长度不够后面补0
   * 
   * @param num
   *          数字
   * @param fixdlenth
   *          字符串长度
   * @return 定长的字符串
   */
  public static Long toFixdLengthStr(long num, int fixdlenth) {
    String fixdLengthStr = String.valueOf(num);
    if (fixdlenth - fixdLengthStr.length() > 0) {
      fixdLengthStr += generateZeroString(fixdlenth - fixdLengthStr.length());
    }
    return Long.parseLong(fixdLengthStr);
  }

  /**
   * 生成一个定长的纯0字符串
   * 
   * @param length
   *          字符串长度
   * @return 纯0字符串
   */
  public static String generateZeroString(int length) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < length; i++) {
      sb.append("0");
    }
    return sb.toString();
  }

  /**
   * 每次生成的len位数都不相同
   * 
   * @param param
   * @return 定长的数字
   */
  public static int getNotSimple(int[] param, int len) {
    Random rand = new Random();
    for (int i = param.length; i > 1; i--) {
      int index = rand.nextInt(i);
      int tmp = param[index];
      param[index] = param[i - 1];
      param[i - 1] = tmp;
    }
    int result = 0;
    for (int i = 0; i < len; i++) {
      result = result * 10 + param[i];
    }
    return result;
  }

}
